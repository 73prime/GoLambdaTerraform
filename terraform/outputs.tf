#Put your output vars here, could go to outputs.tf
#output "primary_rds_endpoint" {
#    value = aws_db_instance.primary.endpoint
#}
output "url" {
  value = "${aws_api_gateway_deployment.time_deploy.invoke_url}${aws_api_gateway_resource.timeresource.path}"
}