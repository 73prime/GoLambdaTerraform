.PHONY: default
default: displayhelp ;

displayhelp:
	@echo Use "clean" or "build" or "deploy" or "destroy" with make, por favor.

build:
	docker build -t amazonlinux:latest .
	docker run -v $(PWD):/host amazonlinux:latest

clean:
	@echo Removing Docker things
	docker rmi amazonlinux:latest -f
	docker image prune -a

deploy:
	@echo Deploying
	cd terraform && terraform apply

destroy:
	@echo Destroy Infra
	cd terraform && terraform destroy

