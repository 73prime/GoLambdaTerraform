FROM amazonlinux:latest
RUN yum install golang -y
RUN yum groupinstall "Development Tools" -y
RUN mkdir -p /host
ENV PATH="/host:${PATH}"
WORKDIR /host
ENTRYPOINT ["buildamazon.sh"]