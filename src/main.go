package main

import (
	"context"
	"encoding/json"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"log"
	"net/http"
	"strings"
	"time"
)

type timeEvent struct {
	Time string `json:"time"`
}

func LambdaHandler(ctx context.Context, request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	body := request.Body
	//log.Printf("request coming in: %s", request)
	log.Printf("body coming in: %s", body)
	parts := strings.Split(body, "=")
	if len(parts) == 2 && strings.ToLower(parts[0]) == "text" {
		log.Printf("text to transform: %s", parts[1])
		//DO THINGS HERE.
	}
	t := timeEvent{Time: time.Now().String()}
	b, err := json.Marshal(t)
	if err != nil {
		return events.APIGatewayProxyResponse{}, err
	}
	return events.APIGatewayProxyResponse{
		StatusCode: http.StatusOK,
		Body:       string(b),
	}, nil
}
func main() {
	lambda.Start(LambdaHandler)
}
