# Example Lambda Deploy
Do you want to use:
- [x] Go?
- [x] AWS Lambda?
- [x] Terraform?

If so, this is a boilerplate that solves some problems.

Your compiled version of your program _needs_ to match the running `glibc` code that AWS Lambda's run. Lucky for us, this is actually in the Amazon Linux2 AMI.  So, other than standing up an EC2, and compiling your code there, or corrupting your file system to get the "right" glibc....how do you compile correctly?

....DOCKER!!!

This Makefile contains some key concepts:
* Docker building
* Docker executing
* Compiling Go program from within docker
* Executing terraform to setup a simple Lambda with a 'GET' handler

## Prereqs
* Terraform
* Docker
* Make

## Instructions for Installation
### Configure remote state for Terraform
* Get a unique string `openssl rand -hex 10`
* Configure .env file with the following information:
```
export TF_VAR_STATE_BUCKET=tf-<yourstring>-time
export TF_VAR_REGION=us-east-1
```
* Source this up: `source .env`
* Login to AWS (CLI), and create the bucket
```
aws s3api create-bucket --bucket $TF_VAR_STATE_BUCKET --region $TF_VAR_REGION
```
### Initialize, Plan, Apply Terraform
* cd into terraform directory: `cd terraform`
* Plan:
```
terraform init --backend-config "bucket=\$TF_VAR_STATE_BUCKET" --backend-config "key=tf-state" --backend-config "region=$TF_VAR_REGION"
```
## Instructions for Building Code, Deploying
* Change directory back to root folder of project.
* Build: `make build`
* Deploy: `make deploy` Note, this assumes you've done the Terraform initing in the prior steps
* You can click the HTTP endpoint displayed to open the GET handler, displaying current time.

## Tearing down
* Clean: `make clean`
* Destroy Infrastructure: `make destroy`
NOTE: There will be a CloudWatch log group still in the infra, it will be called: `CloudWatchLogsLogGroup - /aws/lambda/time`. You may want to clean this up.


